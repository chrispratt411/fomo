# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1484923666.3356383
_enable_loop = True
_template_filename = 'C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/test_dmp/homepage/scripts/index.jsm'
_template_uri = 'index.jsm'
_source_encoding = 'utf-8'
import os, os.path, re, json
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer("$(function() {\r\n  // update the time every 1 second\r\n  window.setInterval(function() {\r\n    $('.browser-time').text('The current browser time is ' + new Date() + '.');\r\n  }, ")
        __M_writer(str( request.urlparams[1] ))
        __M_writer(");\r\n\r\n  //update server time button\r\n  $('#server-time-button').click(function() {\r\n    $('.server-time').load('/homepage/index_time/');\r\n  });\r\n});\r\n")
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"24": 5, "17": 0, "31": 25, "25": 5, "23": 1}, "uri": "index.jsm", "filename": "C:/Users/coolp/Google Drive/IS 411 - Systems Design and Implementation/test_dmp/homepage/scripts/index.jsm", "source_encoding": "utf-8"}
__M_END_METADATA
"""
